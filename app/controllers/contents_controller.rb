require 'uri'
class ContentsController < ApplicationController
  include Scrapper
  before_action :url_valid?, only: :create
  
  def index
    @contents = Content.all
    render json: { status: 200, contents: @contents } and return
  end

  def create
    begin
      h1_tags, h2_tags, h3_tags, links = Scrapper.scrap(params[:url])
      @content = Content.create(h1_tags: h1_tags, h2_tags: h2_tags, h3_tags: h3_tags, links: links)
      render json: { status: 200, conversation: @content } and return
    rescue Exception => e
      render json: {status: 406, message: "#{e.message}"}
    end
  end

  private
  
  def url_valid?
    url = URI.parse(params[:url]) rescue false
    url.kind_of?(URI::HTTP) || url.kind_of?(URI::HTTPS)
  end
end
