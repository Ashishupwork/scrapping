class CreateContents < ActiveRecord::Migration[5.0]
  def change
    create_table :contents do |t|
       t.text :h1_tags, array: true, default: []
       t.text :h2_tags, array: true, default: []
       t.text :h3_tags, array: true, default: []
       t.text :links, array: true, default: []
      t.timestamps
    end
  end
end
