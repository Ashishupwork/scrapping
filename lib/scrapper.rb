require 'mechanize'

module Scrapper
  #pass the url in it to which user want to scrap
  def Scrapper.scrap(url)
    mechanize = Mechanize.new
    page = mechanize.get(url)
    h1_tags = page.search('h1').map(&:text).collect(&:squish)
    h2_tags = page.search('h2').map(&:text).collect(&:squish)
    h3_tags = page.search('h3').map(&:text).collect(&:squish)
    links = page.links.map(&:href).reject{|x| x=="#"}
    
    return h1_tags,h2_tags, h3_tags, links
  end
  
  #Not used this time we'll used it later
  def strip_bad_chars(text)
    text.gsub!(/"/, "'");
    text.gsub!(/\u2018/, "'");
    text.gsub!(/[”“]/, '"');
    text.gsub!(/’/, "'");
    return text
  end
end