## Parse the page content using restful api.

### Usage
Parsing the page content of given Url .
we are parsing the content of page with the tags h1, h2 and h3 and the links of page and save these in our database.


Example:
Instructions how to parse the content of given url.

1. open the postman(chrome extension) and enter the url https://scrapping.herokuapp.com/contents
2. Select POST from Drop down.
3. Add the headers Accept --- application/json and Content-type ----application/json
4. Select raw and Add the parameters in body section
   {"url": "https://ibudtender.com/"}

How to check the previous store contents.

Open the web browser and enter the api url https://scrapping.herokuapp.com/contents.json. You'll see the json data on browser which are previously stored content.